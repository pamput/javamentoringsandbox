package me.sandbox.notes;

public class Scale {
  private Note[] notes;

  public Scale(Note[] notes) {
    this.notes = notes;
  }

  public Note[] getNotes() {
    return notes;
  }
}
