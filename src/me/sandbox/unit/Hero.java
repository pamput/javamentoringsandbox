package me.sandbox.unit;

public class Hero implements Unit{

  private String name;

  public Hero(String name) {
    this.name = name;
  }

  @Override
  public void attack(Unit target) {
    System.out.println(this.name + ": Attack!!!!");
  }

  @Override
  public String getName() {
    return name;
  }
}
