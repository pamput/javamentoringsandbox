package me.sandbox.unit.enemy;

import me.sandbox.unit.Unit;

public abstract class  Enemy implements Unit {

  public abstract String shout();

  @Override
  public void attack(Unit target) {
    System.out.println("Grunt: I miss!");
  }

  @Override
  public String getName() {
    return "Grunt";
  }
}
