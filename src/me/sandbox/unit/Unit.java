package me.sandbox.unit;

public interface Unit {
  void attack(Unit target);
  String getName();
}
