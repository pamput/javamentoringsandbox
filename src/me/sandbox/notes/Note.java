package me.sandbox.notes;

public enum Note {
  Do, Re, Mi, Fa, Sol, La, Si
}
