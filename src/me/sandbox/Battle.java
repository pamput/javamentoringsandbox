package me.sandbox;

import me.sandbox.unit.Unit;
import me.sandbox.unit.enemy.Enemy;

public class Battle {

  private Unit unit1;
  private Unit unit2;

  public Battle(Unit unit1, Unit unit2) {
    this.unit1 = unit1;
    this.unit2 = unit2;
  }

  public void doBattle() {
    System.out.printf("%s VS %s%n", unit1.getName(), unit2.getName());

    if (unit2 instanceof Enemy e) {
      e.shout();
    } else {
      System.out.println("me.paco.Unit 2 is not an enemy! No shout for you");
    }

    unit1.attack(unit2);
    unit2.attack(unit1);
  }
}
