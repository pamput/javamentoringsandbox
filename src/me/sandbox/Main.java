package me.sandbox;

import me.sandbox.unit.Hero;
import me.sandbox.unit.Unit;
import me.sandbox.unit.enemy.Monster;

public class Main {
  public static void main(String[] args) {
    Unit u1 = new Hero("Paco");
    Unit u2 = new Monster();

    Battle battle = new Battle(u2, u1);

    battle.doBattle();
  }
}